package com.devcamp.rest_api.CircleCylinderAPI;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class apiController {
    @GetMapping("/circle-area")
    public double Circle(@RequestParam(value = "circle",defaultValue = "1")double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }
    @GetMapping("/cylinder-volume")
    public double Cylinder(@RequestParam(value = "cylinder",defaultValue = "1")double radius,@RequestParam(value ="height" )double height) {

        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }
}
